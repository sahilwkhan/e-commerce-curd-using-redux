
import { LOAD_PRODUCTS, DELETE_PRODUCTS, ADD_PRODUCT, EDIT_PRODUCT, ADD_TO_CART, DELETE_FROM_CART, CHANGE_CART_QUANTITY } from "./actionTypes";

const initialState = {
    products: [],
    cart: []
};

function products(state = initialState, action) {

    switch (action.type) {

        case LOAD_PRODUCTS: {
            return {
                ...state,
                products: action.payload
            }
        }

        case DELETE_PRODUCTS: {
            return {
                ...state,
                products: state.products.filter((product) => {
                    return product.id !== action.payload
                })
            }
        }


        case ADD_PRODUCT: {
            return {
                ...state,
                products: [action.payload, ...state.products]
            }
        }

        case EDIT_PRODUCT: {
            return {
                products: state.products.map((product) => {
                    if (product.id == action.payload.id) {
                        return action.payload;
                    } else {
                        return product;
                    }
                }),
                cart: state.cart
            }
        }

        case ADD_TO_CART: {
            return {
                ...state,
                cart: [{ ...action.payload, 'quantity': 1 }, ...state.cart]
            }
        }

        case DELETE_FROM_CART: {
            return {
                ...state,
                cart: state.cart.filter((product) => {
                    return product.id !== action.payload;
                })
            }
        }

        case CHANGE_CART_QUANTITY: {


            return {
                ...state,
                cart: state.cart.map((product) => {
                    console.log(product);
                    if (product.id === action.payload.product.id) {
                        let newProduct =  {...action.payload.product,quantity: action.payload.quantity};
                        return newProduct;
                    }
                    else {
                        return product;
                    }
                })
            }
        }


        default: {
            return state;
        }
    }

};

export default products;