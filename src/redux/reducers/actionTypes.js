export const LOAD_PRODUCTS = "LOAD_PRODUCTS";
export const DELETE_PRODUCTS = "DELETE_PRODUCTS";
export const ADD_PRODUCT = "ADD_PRODUCT";
export const EDIT_PRODUCT = "EDIT_PRODUCT";
export const ADD_TO_CART = "ADD_TO_CART";
export const DELETE_FROM_CART = "DELETE_FROM_CART";
export const CHANGE_CART_QUANTITY = "CHANGE_CART_QUANTITY";