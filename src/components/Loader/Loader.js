import React, { Component } from 'react';

import './Loader.css';

class Loader extends Component{

    render(){
        return (
            <div className="loader_container">
                <h1>Your products are loading</h1>
                <div className="lds-spinner">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        );
    }
}

export default Loader;