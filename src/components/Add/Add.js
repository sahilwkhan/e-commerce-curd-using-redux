import { Component, useState } from 'react';
import { v4 as uuidv4 } from "uuid";
import { Link } from 'react-router-dom';
import './Add.css';
import { ADD_PRODUCT } from '../../redux/reducers/actionTypes';
import { connect } from 'react-redux';

class Add extends Component {

    constructor(props) {
        super(props);


        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            error: {
                categoryError: "",
                descriptionError: "",
                priceError: "",
                rateError: "",
                countError: "",
                titleError: "",
                imageError: ""
            },
            formData: {
                category: "",
                description: "",
                id: uuidv4(),
                price: "",
                rating: {
                    rate: 0,
                    count: 0,
                },
                title: "",
                image: ""
            },
            productExistMessage: "",
            productEmptyMessage: "",
            productAddedMessage: ""
        }



    }

    handleChange = (event) => {
        let nameUpdate = event.target.name;
        let valueUpdate = event.target.value;

        if (valueUpdate.length === 0) {
            this.setState({ error: { ...this.state.error, [`${nameUpdate}Error`]: `${nameUpdate} cannot be empty` } });
        } else {
            this.setState({ error: { ...this.state.error, [`${nameUpdate}Error`]: '' } });
        }

        const updatedFormData = {
            ...this.state.formData,
            [nameUpdate]: valueUpdate
        };

        this.setState({ formData: updatedFormData });
    }

    handleRatingChange = (event) => {
        let nameUpdate = event.target.name;
        let valueUpdate = event.target.value;

        if (valueUpdate.length === 0) {
            this.setState({
                error: {
                    ...this.state.error,
                    [`${nameUpdate}Error`]: `${nameUpdate} cannot be empty`,
                },
            });
        } else if (nameUpdate === "rate") {
            if (isNaN(valueUpdate) || parseFloat(valueUpdate) > 5) {
                this.setState({
                    error: {
                        ...this.state.error,
                        [`${nameUpdate}Error`]: `${nameUpdate} must be an Integer or a Decimal value between 0-5`,
                    },
                });
            } else {
                this.setState({
                    error: {
                        ...this.state.error,
                        [`${nameUpdate}Error`]: ``,
                    },
                    formData: {
                        ...this.state.formData,
                        rating: {
                            ...this.state.formData.rating,
                            rate: parseFloat(valueUpdate),
                        },
                    },
                });
            }
        } else if (nameUpdate === "count") {
            if (isNaN(valueUpdate) || valueUpdate.split("").includes(".")) {
                this.setState({
                    error: {
                        ...this.state.error,
                        [`${nameUpdate}Error`]: `${nameUpdate} must be an Integer`,
                    },
                });
            } else {
                this.setState({
                    error: {
                        ...this.state.error,
                        [`${nameUpdate}Error`]: ``,
                    },
                    formData: {
                        ...this.state.formData,
                        rating: {
                            ...this.state.formData.rating,
                            count: parseInt(valueUpdate),
                        },
                    },
                });
            }
        } else {
            this.setState({
                error: {
                    ...this.state.error,
                    [`${nameUpdate}Error`]: ``,
                },
            });
        }
    };



    handleSubmit = (event) => {
        event.preventDefault();

        console.log(this.props, "asdsad");
        const { title, category, description, price, rating: { rate, count } } = this.state.formData;

        const isProductAdded = this.props.products.some(product => (
            product.title === title &&
            product.category === category &&
            product.description === description &&
            product.price === price &&
            product.rating.rate === rate &&
            product.rating.count === count
        ));

        const emptyField = () => {
            const emptyItems = Object.values(this.state.formData).filter((inputValue) => {
                return (inputValue.length === 0);
            })
            console.log(emptyItems);
            return (emptyItems.length > 0);
        }

        if (isProductAdded) {
            console.log(this.props.products.apiData);
            this.setState({productAddedMessage: ""});
            this.setState({productEmptyMessage: ""});
            this.setState({productExistMessage: "Given item is already added in Cart."});
        }
        else if (emptyField()) {
            this.setState({productExistMessage: ""});
            this.setState({productAddedMessage: ""});
            this.setState({productEmptyMessage: "All input for product must be entered."});
        }
        else {
            this.setState({productEmptyMessage: ""});
            this.props.addProduct(this.state.formData);
            this.setState({productAddedMessage: `${this.state.formData.title} added in product List.`});

            this.setState({
                formData: {
                    category: "",
                    description: "",
                    id: uuidv4(),
                    price: "",
                    rating: {
                        rate: "",
                        count: "",
                    },
                    title: "",
                    image: ""
                }
            });

        }
    }


    render() {

        return (
            <div className="add-page">

                <form className="add-details" onSubmit={this.handleSubmit}>
                    <div className="add-heading">
                        Add a Product
                    </div>

                    {
                        this.state.productExistMessage &&
                        <h2 className='productExistMessage'>{this.state.productExistMessage}</h2>
                    }
                    {
                        this.state.productEmptyMessage &&
                        <h2 className='productEmptyMessage'>{this.state.productEmptyMessage}</h2>
                    }
                    {
                        this.state.productAddedMessage &&
                        <h2 className='productAddMessage'>{this.state.productAddedMessage}</h2>
                    }


                    <div className="add-data">
                        <div className="add-title">
                            Product category
                        </div>
                        <input type="text" name="category" onChange={this.handleChange} value={this.state.formData.category} className="add-input" />
                    </div>
                    {this.state.error.categoryError && <div className='error'>{this.state.error.categoryError}</div>}

                    <div className="add-data add-description">
                        <div className="add-title">
                            Product description
                        </div>
                        <input type="text" name="description" onChange={this.handleChange} value={this.state.formData.description} className="add-input" />
                    </div>
                    {this.state.error.descriptionError && <div className='error'>{this.state.error.descriptionError}</div>}

                    <div className="add-data">
                        <div className="add-title">
                            Product image
                        </div>
                        <input type="text" name="image" onChange={this.handleChange} value={this.state.formData.image} className="add-input" />
                    </div>
                    {this.state.error.imageError && <div className='error'>{this.state.error.imageError}</div>}

                    <div className="add-data">
                        <div className="add-title">
                            Product price
                        </div>
                        <input type="text" name="price" onChange={this.handleChange} value={this.state.formData.price} className="add-input" />
                    </div>
                    {this.state.error.priceError && <div className='error'>{this.state.error.priceError}</div>}


                    <div className="add-data">
                        <div className="add-title">
                            Product title
                        </div>
                        <input type="text" name="title" onChange={this.handleChange} value={this.state.formData.title} className="add-input" />
                    </div>
                    {this.state.error.titleError && <div className='error'>{this.state.error.titleError}</div>}

                    <div className="add-data">
                        <div className="add-title">
                            Product Rating
                        </div>
                        <input type="text" name="rate" onChange={this.handleRatingChange} value={this.state.formData.rating.rate} className="add-input" />
                    </div>
                    {this.state.error.rateError && <div className='error'>{this.state.error.rateError}</div>}

                    <div className="add-data">
                        <div className="add-title">
                            Product User Count
                        </div>
                        <input type="text" name="count" onChange={this.handleRatingChange} value={this.state.formData.rating.count} className="add-input" />
                    </div>
                    {this.state.error.countError && <div className='error'>{this.state.error.countError}</div>}


                    <button type="submit" className="add-submit-button" >
                        Submit
                    </button>

                </form>
            </div>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        addProduct: (product) =>
            dispatch({
                type: ADD_PRODUCT,
                payload: product,
            }),
    };
};

export default connect(null, mapDispatchToProps)(Add);