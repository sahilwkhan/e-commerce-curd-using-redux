import React from 'react';
import { connect } from 'react-redux';
import { CHANGE_CART_QUANTITY, DELETE_FROM_CART } from '../../redux/reducers/actionTypes';
import './Cart.css'

class Cart extends React.Component {

    constructor(props) {
        super(props);

        this.CART_STATE = {
            EMPTY: 'empty',
            LOADED: 'loaded'
        }
        // console.log(this.props, "CART PRODUCTS");
    }

    handleCartDelete = (product) => {
        this.props.deleteFromCart(product.id);
    }

    handleQuantityIncrease = (productArray, quantity) => {
        if (quantity > 0) {
            this.props.changeCartQuantity(productArray, quantity);
        }
    }

    // constcartProductsDetails = () => {
    //     this.props.cart.reduce((totalNumber, product) => {
    //         totalNumber += product.quantity;
    //         return totalNumber;
    //     }, 1)

    //     // this.state(())
    // }


    render() {
        return (
            <div>


                {
                    !(this.props.cart) &&
                    <h1 className='emptyCartMessage'>No Products added in your cart.</h1>
                }
                {
                    (this.props.cart) &&
                    this.props.cart.length === 0 &&
                    <h1 className='emptyCartMessage'>No Products added in your cart.</h1>
                }

                {
                    (this.props.cart) &&
                    this.props.cart.length > 0 &&
                    <div className='cart-info'>
                        <div className='product-container'>
                            {
                                this.props.cart.map((productArray) => {
                                    return (
                                        <div key={productArray.id} className="product-card">
                                            <img
                                                className='productImages'
                                                key={productArray.id} src={productArray.image}
                                                alt={productArray.title}
                                            />
                                            <div className="title">
                                                {productArray.title}
                                            </div>
                                            <div className="category">
                                                Category : {productArray.category}
                                            </div>

                                            <div className="price">
                                                ${productArray.price}
                                            </div>

                                            <div className="rating">
                                                <div>
                                                    <i className="fa-solid fa-star rate-star"></i>
                                                    {productArray.rating.rate} / 5
                                                </div>
                                                <div>
                                                    <i className='fas fa-users'></i>
                                                    {productArray.rating.count}
                                                </div>
                                            </div>

                                            <div className="cart-button"
                                                onClick={() => this.handleCartDelete(productArray)}>
                                                Delete from Cart <i className='fa fa-trash'></i>
                                            </div>

                                            <div className="quantity">
                                                <i className="fa fa-minus-circle quantity-icon" aria-hidden="true" onClick={() => this.handleQuantityIncrease(productArray, productArray.quantity - 1)}></i>
                                                {productArray.quantity}
                                                <i className="fa fa-plus-circle quantity-icon" aria-hidden="true" onClick={() => this.handleQuantityIncrease(productArray, productArray.quantity + 1)}></i>

                                            </div>
                                            {/* 

                                            <div className="total-Price">
                                                Total Item Price : {parseFloat((productArray.quantity * productArray.price).toFixed(2))}
                                            </div>

                                           
                                            */}

                                        </div>
                                    )
                                })
                            }
                        </div>
                        <div className='cart-total'>
                            <div className='priceDetails'>
                                Cart Details
                            </div>
                            <div className='cartCount'>
                                <span>
                                    Cart Products
                                </span>
                                <span>
                                    {this.props.cart.length}
                                </span>
                            </div>
                            {/* 
                            <div className='totalCount'>
                                <span>
                                    Total Cart Quantity
                                </span>
                                <span>
                                    {this.cartProductsDetails}
                                </span>
                            </div>
                            <div className='discount'>
                                <span>
                                    Total Cart Price
                                </span>
                                <span>
                                    {allProductsPrice}
                                </span>
                            </div>
                            <div className='delivery'>
                                <span>
                                    Shipping Charges
                                </span>
                                <span>
                                    ${cartData.length * 2}
                                </span>
                            </div>
                            <div className='total-amount'>
                                <span>
                                    Total Amount
                                </span>
                                <span>
                                    ${allProductsPrice + cartData.length * 2}
                                </span>
                            </div> */}
                        </div>
                    </div>
                }

            </div>
        )

    }
}


const mapDispatchToProps = (dispatch) => {

    return {
        deleteFromCart: (product) =>
            dispatch({
                type: DELETE_FROM_CART,
                payload: product,
            }),

        changeCartQuantity: (product, quantity) =>
            dispatch({
                type: CHANGE_CART_QUANTITY,
                payload: { product, quantity }
            })
    };
}

export default connect(null, mapDispatchToProps)(Cart);