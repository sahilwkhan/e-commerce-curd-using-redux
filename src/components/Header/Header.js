import React, { Component } from 'react';

import './Header.css';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
class Header extends Component {


    render() {
        return (
            <header className="headerStyle">
                <div className='logo'>
                    <img
                        className='logoStyle'
                        src='https://cdn.dribbble.com/users/3267379/screenshots/6098927/e_shop.jpg'
                        alt="store-logo"
                    />
                    <div>Fake Store</div>
                </div>
                <div className='accountOptions'>
                    <Link to='/' className='header-btn add-product-button home' >
                        Home
                    </Link>
                    <Link to='/add' className='header-btn add-product-button' >
                        Add Product
                    </Link>

                    <Link to='/cart' className='header-btn add-button' >
                        Cart {(this.props.cart) &&
                            this.props.cart.length}
                    </Link>


                </div>

            </header>

        );

    }
}

const mapStateToProps = (state) => {

    return {
        products: state.store.products,
        cart: state.store.cart
    };
};

export default connect(mapStateToProps)(Header);