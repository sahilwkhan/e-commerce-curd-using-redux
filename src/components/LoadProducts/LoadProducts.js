import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import './LoadProducts.css';
import { ADD_TO_CART, DELETE_FROM_CART, DELETE_PRODUCTS } from '../../redux/reducers/actionTypes';

class LoadProducts extends Component {


    state = {
        inCartMessage: "",
    }

    handleDelete = (id) => {
        this.setState({ inCartMessage: "" })
        const isDeletedProductInCart = this.props.cart.filter((cartProduct) => {
            if (cartProduct.id === id) {
                return true;
            }
            else {
                return false;
            }
        })

        if (isDeletedProductInCart.length > 0) {
            this.props.deleteFromCart(id);
        }
        this.props.deleteProduct(id);
    };

    handleCart = (product) => {
        this.setState({ inCartMessage: "" })
        const isProductInCart = this.props.cart.filter((cartProduct) => {
            if (cartProduct.id == product.id) {

                return cartProduct;
            }
        })
        if (isProductInCart.length === 0) {
            this.props.addToCart(product);
            this.setState({ inCartMessage: "" })
            console.log("Product Added to cart.");
        }
        else {
            this.setState({ inCartMessage: `${product.title} is already added in the Cart.` });
            console.log("Product Already Added.");

        }
    }


    render() {
        return (
            <>
                {(this.state.inCartMessage) && <h2 className='inCartMessage'>{this.state.inCartMessage}</h2>}
                <div className="product-container">

                    {this.props.products.map((productArray) => {
                        return (
                            <div key={productArray.id} className="product-card">
                                <Link to={`/product/${productArray.id}`}>
                                    <img
                                        className="productImages"
                                        src={productArray.image}
                                        alt={productArray.title}
                                    />
                                </Link>

                                <div className="title">
                                    <div>{productArray.title}</div>
                                </div>

                                <div className="category">
                                    Category: {productArray.category}
                                </div>

                                <div className="price">${productArray.price}</div>

                                <div className="rating">
                                    <div>
                                        <i className="fa-solid fa-star rate-star"></i>
                                        {productArray.rating.rate} / 5
                                    </div>
                                    <div>
                                        <i className="fas fa-users"></i>
                                        {productArray.rating.count}
                                    </div>
                                </div>

                                <div className="cart-button"
                                    onClick={() => this.handleCart(productArray)}
                                >
                                    Add to cart
                                    <i className="fa-solid fa-cart-plus cart-icon"></i>
                                </div>

                                <div className="curd-button">
                                    <Link
                                        to={`/edit/${productArray.id}`}
                                        className="edit curd-hover"
                                    >
                                        Edit
                                    </Link>

                                    <div
                                        className="delete curd-hover"
                                        onClick={() => this.handleDelete(productArray.id)}
                                    >
                                        Delete
                                    </div>
                                </div>

                            </div>
                        );
                    })}
                </div>
            </>
        );
    }
}




const mapDispatchToProps = (dispatch) => {
    return {
        addToCart: (product) =>
            dispatch({
                type: ADD_TO_CART,
                payload: product
            }),

        deleteFromCart: (product) =>
            dispatch({
                type: DELETE_FROM_CART,
                payload: product
            }),

        deleteProduct: (id) =>
            dispatch({
                type: DELETE_PRODUCTS,
                payload: id
            }),

    };
};

export default connect(null, mapDispatchToProps)(LoadProducts);
