import { Component } from 'react';

import './Edit.css';


import { connect } from 'react-redux';
import { EDIT_PRODUCT } from '../../redux/reducers/actionTypes';


class Edit extends Component {
    constructor(props) {
        super(props);

        const originalProductID = window.location.href.split('/')[4];

        const originalProductList = this.props.products.find((product) => {
            return (product.id == originalProductID);
        })

        console.log(originalProductList);

        const originalProduct = originalProductList;

        this.state = {
            error: {
                categoryError: "",
                descriptionError: "",
                priceError: "",
                rateError: "",
                countError: "",
                titleError: "",
                imageError: "",
            },
            originalProduct : originalProduct,
            editformData: {
                category: originalProduct.category,
                description: originalProduct.description,
                id: originalProductID,
                image: originalProduct.image,
                price: originalProduct.price,
                rating: {
                    rate: originalProduct.rating.rate,
                    count: originalProduct.rating.count,
                },
                title: originalProduct.title

            },
            isProductAdded: false

        };
    }




    handleChange = (event) => {
        console.log(this.props,"EDIT");
        const { name, value } = event.target;
        const { editformData } = this.state;

        if (value.length === 0) {
            this.setState({
                error: {
                    ...this.state.error,
                    [`${name}Error`]: `${name} cannot be empty`
                }
            });
        }
        else {
            this.setState({
                error: {
                    ...this.state.error,
                    [`${name}Error`]: ``
                }
            });
        }

        const updatedFormData = {
            ...editformData,
            [name]: value
        };
    
        console.log(updatedFormData,"Edit Updated form");
        this.setState({
            editformData: updatedFormData,
        });
    
        this.props.editProduct(updatedFormData);
    }

    handleEditRatingChange = (event) => {
        const { name, value } = event.target;
        const { editformData } = this.state;

        if (value.length === 0) {
            this.setState({
                error: {
                    ...this.state.error,
                    [`${name}Error`]: `${name} cannot be empty`
                }
            });
        } else if (name === 'rate') {
            if (isNaN(value) || parseFloat(value) > 5) {
                this.setState({
                    error: {
                        ...this.state.error,
                        [`${name}Error`]: `${name} must be an Integer or a Decimal value between 0-5`
                    }
                });
            } else {
                this.setState({
                    error: {
                        ...this.state.error,
                        [`${name}Error`]: ``
                    }
                });
            }
        } else if (name === 'count') {
            if (isNaN(value) || value.split('').includes(".")) {
                this.setState({
                    error: {
                        ...this.state.error,
                        [`${name}Error`]: `${name} must be an Integer`
                    }
                });
            } else {
                this.setState({
                    error: {
                        ...this.state.error,
                        [`${name}Error`]: ``
                    }
                });
            }
        } else {
            this.setState({
                error: {
                    ...this.state.error,
                    [`${name}Error`]: ``
                }
            });
        }

        this.setState({
            editformData: {
                ...editformData,
                rating: {
                    ...editformData.rating,
                    [name]: value
                }
            }
        });
    }


    handleEditSubmit = (event) => {
        event.preventDefault();
        this.props.editProduct(this.state.editformData);
    }

    render() {

        return (
            <div className="edit-page" >

                <form className="edit-details" onSubmit={this.handleEditSubmit}>
                    <div className='original-product'>
                        <div key={this.state.originalProduct.id} className="product-card">

                            <div to={`/product/${this.state.originalProduct.id}`} >
                                <img
                                    className='productImages'
                                    key={this.state.editformData.id} src={this.state.editformData.image}
                                    alt={this.state.editformData.title}
                                />
                            </div>

                            <div className="title">
                                {this.state.editformData.title}
                            </div>

                            <div className="category">
                                Category : {this.state.editformData.category}
                            </div>

                            <div className="price">
                                ${this.state.editformData.price}
                            </div>

                            <div className="rating">
                                <div>
                                    <i className="fa-solid fa-star rate-star"></i>
                                    {this.state.editformData.rating.rate} / 5
                                </div>
                                <div>
                                    <i className='fas fa-users'></i>
                                    {this.state.editformData.rating.count}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="edit-heading">
                        Edit Product
                    </div>

                    <div className="edit-data">
                        <div className="edit-title">
                            Product category
                        </div>
                        <input
                            type="text"
                            name="category"
                            onChange={this.handleChange}
                            value={this.state.editformData.category}
                            className="add-input"
                        />
                    </div>
                    {this.state.error.categoryError && <div className='error'>{this.state.error.categoryError}</div>}

                    <div className="edit-data add-description">
                        <div className="edit-title">
                            Product description
                        </div>
                        <input type="text"
                            name="description"
                            onChange={this.handleChange}
                            value={this.state.editformData.description}
                            className="add-input"
                        />
                    </div>
                    {this.state.error.descriptionError && <div className='error'>{this.state.error.descriptionError}</div>}


                    <div className="edit-data">
                        <div className="edit-title">
                            Product image
                        </div>
                        <input
                            type="text"
                            name="image"
                            onChange={this.handleChange}
                            value={this.state.editformData.image}
                            className="add-input"
                        />
                    </div>
                    {this.state.error.imageError && <div className='error'>{this.state.error.imageError}</div>}



                    <div className="edit-data">
                        <div className="edit-title">
                            Product price
                        </div>
                        <input
                            type="text"
                            name="price"
                            onChange={this.handleChange}
                            value={this.state.editformData.price}
                            className="edit-input"
                        />
                    </div>
                    {this.state.error.priceError && <div className='error'>{this.state.error.priceError}</div>}


                    <div className="edit-data">
                        <div className="edit-title">
                            Product title
                        </div>
                        <input
                            type="text"
                            name="title"
                            onChange={this.handleChange}
                            value={this.state.editformData.title}
                            className="add-input"
                        />
                    </div>
                    {this.state.error.titleError && <div className='error'>{this.state.error.titleError}</div>}


                    <div className="edit-data">
                        <div className="edit-title">
                            Product Rating
                        </div>
                        <input
                            type="text"
                            name="rate"
                            onChange={this.handleEditRatingChange}
                            value={this.state.editformData.rating.rate}
                            className="add-input"
                        />
                    </div>
                    {this.state.error.rateError && <div className='error'>{this.state.error.rateError}</div>}


                    <div className="edit-data">
                        <div className="edit-title">
                            Product User Count
                        </div>
                        <input
                            type="text"
                            name="count"
                            onChange={this.handleEditRatingChange}
                            value={this.state.editformData.rating.count}
                            className="add-input"
                        />
                    </div>
                    {this.state.error.countError && <div className='error'>{this.state.error.countError}</div>}



                    <button type="submit" className="edit-submit-button" >
                        Submit
                    </button>

                </form>
            </div>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        editProduct: (product) =>
            dispatch({
                type: EDIT_PRODUCT,
                payload: product,
            }),
    };
};


export default connect(null, mapDispatchToProps)(Edit);