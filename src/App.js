import axios from 'axios';
import { Component } from 'react';

import './App.css';
import Header from './components/Header/Header';
import Loader from './components/Loader/Loader';
import LoadProducts from './components/LoadProducts/LoadProducts';
import Add from './components/Add/Add';
import Edit from './components/Edit/Edit';
import Cart from './components/Cart/Cart';

import { connect } from 'react-redux';
import { Route, Routes } from "react-router-dom";


class App extends Component {


  constructor(props) {
    super(props);

    this.API_STATES = {
      LOADING: "loading",
      LOADED: "loaded",
      ERROR: "error",
    };

    this.state = {
      status: '',
      productToEdit: null
    };
    // console.log(this.props,"App construtor");
  }



  componentDidMount() {

    this.setState({
      status: this.API_STATES.LOADING,
    }, () => {
      axios
        .get('https://fakestoreapi.com/products')
        .then((response) => {

          this.props.loadProducts(response.data);
          this.setState({
            status: this.API_STATES.LOADED,
          });

        })
        .catch((error) => {
          console.error("Error loading products");
          this.setState({
            status: this.API_STATES.ERROR,
          })
        })
    })

  }

  render() {

    return (
      <div className="App">
        <Header cart={this.props.cart}/>

        {
          this.state.status === this.API_STATES.LOADING &&
          <div className='loaderDisplay'>
            <Loader />
          </div>
        }

        {
          this.state.status === this.API_STATES.ERROR &&
          <div className='errorDisplay'>
            Unable to fetch products. Try again later
          </div>
        }

        {
          this.state.status === this.API_STATES.LOADED &&
          this.props.products.length === 0 &&
          <div className='emptyErrorDisplay'>
            No Products Available
          </div>
        }

        {
          this.state.status === this.API_STATES.LOADED && this.props.products.length > 0 &&

          <Routes>
            <Route path="/" element={<LoadProducts products={this.props.products}  cart={this.props.cart}/>} />
            <Route path="/add" element={<Add products={this.props.products} />} />
            <Route path="/edit/:productId" element={<Edit products={this.props.products}/>} />
            <Route path="/cart" element={<Cart cart={this.props.cart}/>} /> 
          </Routes>

        }

      </div>
    );
  }
}


const mapStateToProps = (state) => {

  return {
    products: state.store.products,
    cart: state.store.cart
  };
};


const mapDispatchToProps = (dispatch) => {

  return {
    loadProducts: (products) => dispatch({
      type: 'LOAD_PRODUCTS',
      payload: products
    })
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(App);
